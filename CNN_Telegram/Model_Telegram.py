import telebot
import cv2
import numpy as np
import keras
import os
bot = telebot.TeleBot('1345008686:AAGzCgEIdJG2PNEp82DRQjX6nYDdbmZBdQQ')
model = keras.models.load_model('C:\\Users\\maksi\\Python\\MyFirstModel.h5')
model.load_weights('C:\\Users\\maksi\\Python\\ConvNN.h5')
def neural_network(name):
    data = []
    image = cv2.imread('C:\\Users\\maksi\\Python\\' + name)
    image = cv2.resize(image, (32,32))
    data.append(image)
    data = np.array(data, dtype = 'float') / 255.0
    predict = model.predict(data)
    k = 0
    total = ''
    predict = predict[0]
    for i in range(len(predict)):
    	if predict[i] == max(predict):
    		k = i
    if k == 0:
    	total = 'Ромашка'
    elif k == 1:
    	total = 'Одуванчик'
    elif k == 2:
    	total = 'Роза'
    elif k == 3:
    	total = 'Подсолнух'
    elif k == 4:
    	total = 'Тюльпан'
    return total
@bot.message_handler(content_types=['photo'])
def send_text(message):
	if message.text == 'Привет':
		bot.send_message(message.chat.id, 'Привет, мой создатель')
	elif message.text == 'Пока':
		bot.send_message(message.chat.id, 'Прощай, создатель')
	print(message.photo)
	raw = message.photo[1].file_id
	path = raw+".jpg"
	file_info = bot.get_file(raw)
	downloaded_file = bot.download_file(file_info.file_path)
	with open(path,'wb') as new_file:
	       new_file.write(downloaded_file)
	pred = neural_network(path)
	bot.send_message(message.chat.id, pred)
	os.remove(path)
bot.infinity_polling(True)
