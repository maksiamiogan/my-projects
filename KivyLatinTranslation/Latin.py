import codecs
import ast
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import pymorphy2
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.anchorlayout import AnchorLayout
from kivy.graphics import *
trans = 'Перевод'
class LogoBackground(Label):
	def on_size(self, *args):
		self.canvas.before.clear()
		with self.canvas.before:
			Color(1,.71,.35,1)
			Rectangle(pos=self.pos, size=self.size)
class MyApp(App):
	def translate(self, instance):
		try:
			morph = pymorphy2.MorphAnalyzer()
			d = codecs.open( "data.txt", "r", "utf-8")
			d2 = codecs.open( "data3.txt", "r", "utf-8")
			data = d.read()
			data = ast.literal_eval(data)
			data2 = d2.read()
			data2 = ast.literal_eval(data2)
			search = input_text.text
			search = search.lower()
			check = False
			for i in range(len(data2)):
				search1 = search.replace(' ','')
				data2[i][1] = data2[i][1].replace(' ','')
				if search1 == data2[i][1].lower():
					total1 = data2[i][0]
					check = True
			search = search.split()
			perc = 0
			final = []
			rech = ''
			word_sex = [0,0]
			total=[]
			for j in range(len(search)):
				for i in range(len(data)):
					if fuzz.token_sort_ratio(search[j], data[i][0]) > perc:
						perc = fuzz.token_sort_ratio(search[j], data[i][0])
						ans = data[i][1]
						morph1 = morph.parse(ans)[0]
						rech = morph1.tag.POS
					elif fuzz.token_sort_ratio(search[j], data[i][0]) == 100:
						ans = data[i][1]
						morph1 = morph.parse(ans)[0]
						rech = morph1.tag.POS
						if rech == 'NOUN':
							rech = ''
							word_sex[0] = (ans)
							word_sex[1] = (morph1.tag.gender)
						continue
				if rech == 'NOUN':
					rech = ''
					word_sex[0] = (ans)
					word_sex[1] = (morph1.tag.gender)
				perc = 0
				final.append(ans)
			k=0
			
			for i in range(len(final)):
				final[i] = final[i].replace(' ','')
			if len(final) > 1:
				for i in range(len(final)):
					morph = pymorphy2.MorphAnalyzer()
					morph2 = morph.parse(final[i])[0]
					if morph2.tag.POS != 'NOUN':
						result = morph2.inflect({word_sex[1], 'nomn'})
						final[i] = result.word
						total.append(result.word)
					else:
						k = final.index(final[i])
				total.append(final[k])
				trans = ''
				if check == False:
					for i in range(len(total)):
						trans += total[i] + ' '
				else:
					trans = total1
			elif len(final) == 1:
				trans = final[0]
			d.close()
		except Exception:
			trans = 'ОшибОчка :( Возможно перевод не найден. Свяжитесь с Максом'
		self.output_text.text = str(trans)

	def build(self):
		al = AnchorLayout()
		bl = BoxLayout(orientation='vertical')
		btn = Button(text='Перевести', on_press=self.translate, background_normal ='',  font_size=30, background_color = (1,.45,0,1))
		global input_text
		input_text = TextInput(multiline=False, text='Введите текст', font_size = 25,background_normal ='', background_color = (1,.71,.35,1))
		self.output_text = LogoBackground(text=trans, font_size = 25)

		bl.add_widget(input_text)
		bl.add_widget(btn)
		bl.add_widget(self.output_text)
		
		al.add_widget(bl)
		return al
	
if __name__ == "__main__":
	MyApp().run()

