import time
import vk_api
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from vk_api.utils import get_random_id
from vk_api.longpoll import VkLongPoll, VkEventType
import datetime
import bs4, requests
import wikipedia
import random
import yandex_weather_api
import xlrd, xlwt
import json
from PIL import ImageFont, ImageDraw, Image

# Useful things
wikipedia.set_lang("RU")
otvet=''
x=''
ans = []
phr = [', потому что так звезды сошлись',', потому что самый(ая) красивый(ая)',', потому что так надо', ', потому что я так сказал!',', потому что... ну пожалуйста',', потому что это рандом',', потому что не гомосексуалист']
ege = [31,30,31,31,29,31,30,31,30]
sub = ['география','География', 'литература','Литература', 'информатика','Информатика','русский','Русский','математика','Математика','история','История','физика','Физика','обществознание','Обществознание','химия','Химия','английский','Английский','французский','Французский', 'немецкий','Немецкий', 'испанский','Испанский', 'китайский','Китайский','биология','Биология']

# SubFunctions
def get_button(label,color,payload=''):
    pass
    return {
      'action': {
          'type': 'text',
          'payload': json.dumps(payload),
          'label': label
      },
      "color": color
  }
keyboard = {
  "one_time": False,
  "buttons": [[
  get_button(label = 'Звонок', color = 'positive')
  ]
  ]
}
keyboard = json.dumps(keyboard,ensure_ascii = False).encode('utf-8')
keyboard = str(keyboard.decode('utf-8'))
keyboard2 = {
  "one_time": True,
  "buttons": [[
  get_button(label = 'Звонок', color = 'positive')
  ]
  ]
}
keyboard2 = json.dumps(keyboard2,ensure_ascii = False).encode('utf-8')
keyboard2 = str(keyboard2.decode('utf-8'))
#Shedule 
def excel(name):
  otvet=[]
  s = 202
  n = datetime.datetime.now()
  weekday = datetime.date.weekday(n)
  weekday+=1
  rb = xlrd.open_workbook('../Maksiam/tt.xls',formatting_info=True)
  sheet = rb.sheet_by_index(0)
  val = sheet.row_values(3)[1]
  n = str(n)
  date, time = n.split()
  hour,minute,second = time.split(':')
  hour=int(hour) + 5
  minute = int(minute)
  for i in range(3,73):
    val = sheet.row_values(i)[1]
    if name in val:
      s = i
  if (hour * 60) + minute >= 930 and weekday != 6 and weekday != 7 and weekday != 5:
      f = 'Расписание на следующий день:'
      otvet.append(f)
      weekday +=1
  if s == 202:
      o = 'Такого ученика не существует в базе данных'
      weekday = 202
  if weekday == 1:
    for i in range(17,23):
      val = sheet.row_values(s)[i]
      val = str(val)
      otvet.append(val)
  if weekday == 2:
    for i in range(26,33):
      val = sheet.row_values(s)[i]
      val = str(val)
      otvet.append(val)
  if weekday == 3:
    for i in range(35,43):
      val = sheet.row_values(s)[i]
      val = str(val)
      otvet.append(val)
  if weekday == 4:
    for i in range(44,52):
      val = sheet.row_values(s)[i]
      val = str(val)
      otvet.append(val)
  if weekday == 5:
    for i in range(53,61):
      val = sheet.row_values(s)[i]
      val = str(val)
      otvet.append(val)
  if weekday == 6 or weekday == 7:
    k = 'Рабочая неделя окончена \m/. Это расписание на понедельник: \n'
    otvet.append(k)
    for i in range(17,23):
      val = sheet.row_values(s)[i]
      val = str(val)
      otvet.append(val)
  if weekday <= 7:
    o = '\n'.join(otvet)
  return o
def rand_music():
  x=0
  x = random.randint(456239017, 456241281)
  return str(x)
def rand_video():
  x=0
  x = random.randint(456239020, 456240768)
  return str(x)
  x=0
  x = random.randint(456239017, 456241281)
  return str(x)
def weather():
    x = yandex_weather_api.get(requests, "3d7de6e0-b1a3-4742-b37d-6088a6e039c9", lat=54.48, lon=53.49)
    f1 = ('Температура ', x['fact']['temp'])
    f2 = ('По ощущениям ', x['fact']['feels_like'])
    f3 = ('Скорость ветра', x['fact']['wind_speed'],'м/с')
    f4 = ('Скорость порыва ветра', x['fact']['wind_gust'],'м/с')
    f5 = ('Рассвет ',x['forecast'][0]['sunrise'] )
    f6 = ('Закат',x['forecast'][0]['sunset'])
    return f1,f2,f3,f4,f5,f6
def timetable():
    otvet = ''
    x=''
    n = datetime.datetime.now()
    weekday = datetime.date.weekday(n)
    n = str(n)
    date, time = n.split()
    hour,minute,second = time.split(':')
    year, month, day = date.split('-')
    weekday+=1
    hour=int(hour) + 5
    minute = int(minute)
    date_base = [["8.30","9.10","9.20","10.00","10.20","11.00","11.20","12.00","12.15","12.55","13.10","13.50","14.00","14.40","14.55","15.35"]]
    if weekday <= 5:
        for i in range (15):
            hour1, minute1 = date_base[0][i].split('.')
            hour2, minute2 = date_base[0][i+1].split('.')
            hour1= int(hour1)
            hour2=int(hour2)
            minute2=int(minute2)
            minute1=int(minute1)
            time1 = hour1 * 60 + minute1
            time2 = hour2*60 + minute2
            time = hour*60 + minute
            if time <= time2 and time >= time1:
                otvet = ('Осталось '+str(time2 - time)+' м'+'\n'+"Прошло "+ str(time - time1) + ' м')
                break
            if time > 930:
                otvet = ('Уроки закончились')
                break

    elif weekday == 7:
        otvet = ('Sunday Funday')
    return str(otvet)
def rand_pict():
  x=0
  x = random.randint(456251350, 456253335)
  return str(x)
def wiki(x):
    f = ''
    try:
        str(wikipedia.summary(x))
    except Exception:
        f = 'Неверный запрос.Попробуйте уточнить или проверить орфографию'
    if f != 'Неверный запрос.Попробуйте уточнить или проверить орфографию':
        f = str(wikipedia.summary(x))
    return f
def _get_user_name_from_vk_id(user_id):
    request = requests.get("https://vk.com/id"+str(user_id))
    bs = bs4.BeautifulSoup(request.text, "html.parser")

    user_name = _clean_all_tag_from_str(bs.findAll("title")[0])
    print(user_name)
    return user_name.split()[0]
def _clean_all_tag_from_str(string_line):
    result = ""
    not_skip = True
    for i in list(string_line):
        if not_skip:
            if i == "<":
                not_skip = False
            else:
                result += i
        else:
            if i == ">":
                not_skip = True

    return result
def answers(x):
    ans = x.split()
    return ans
#Days before exam
def ex(s):
    n = datetime.datetime.now()
    n = str(n)
    date, time = n.split()
    year, month, day = date.split('-')
    month = int(month)
    day = int(day)
    if s == 'география' or s == 'География' or s == 'литература' or s == 'Литература' or s == 'информатика' or s == 'Информатика':
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 8])-6))
        otv = 'До ЕГЭ по предмету ' + s + ': ' + g
    elif s == 'русский' or s == 'Русский':
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 8])-3))
        otv = 'До ЕГЭ по предмету ' + s + ': ' + g
    elif s == 'математика' or s == 'Математика':
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 9])-29))
        otv = 'До ЕГЭ по предмету ' + s + ': ' + g
    elif s == 'история' or s == 'История' or s == 'физика' or s == 'Физика':
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 9])-26))
        otv = 'До ЕГЭ по предмету ' + s + ': ' + g
    elif s == 'обществознание' or s == 'Обществознание' or s == 'химия' or s == 'Химия':
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 9])-22))
        otv = 'До ЕГЭ по предмету ' + s + ': ' + g
    elif s == 'английский' or s == 'Английский' or s == 'французский' or s == 'немецкий' or s == 'испанский' or s == 'китайский':
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 9])-19))
        otv = 'Письменная часть: '+ g + '\n'
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 9])-15))
        otv += 'Устная часть: '+ g + '\n'
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 9])-14))
        otv += 'Устная часть: '+ g
    elif s == 'биология' or s == 'Биология':
        g = str((ege[month - 10] - day) + (sum(ege[month - 9: 9])-19))
        otv = 'До ЕГЭ по предмету биология: ' + g
    return str(otv)

# Main function
def main():
        vk_session = vk_api.VkApi(token='8d3b989c7850fcebed576377c3875dd6728a558f878c114bde98f8b480cbaa8e3eb94e6bff062758b4656')
        vk = vk_session.get_api()
        longpoll = VkBotLongPoll(vk_session, '167530128')
        for event in longpoll.listen():
                if event.obj.text == 'Звонок' or event.obj.text == 'звонок':
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=timetable(),
                        )
                elif event.obj.text == 'Ввод' or event.obj.text == 'ввод':
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=("Вводите ответы"),
                        )
                    q = event.obj.peer_id
                    for event in longpoll.listen():
                        if event.type == VkBotEventType.MESSAGE_NEW:
                            if event.obj.peer_id == q:
                                vk.messages.send(
                                        random_id=get_random_id(),
                                        peer_id = q,
                                        message= ('Готово')
                                        )
                                ans = answers(event.obj.text)
                                break
                elif event.obj.text == 'Проверка' or event.obj.text == 'проверка':
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=("Вводите ответы"),
                        )
                    q = event.obj.peer_id
                    for i in range(len(ans)):
                        for event in longpoll.listen():
                            if event.type == VkBotEventType.MESSAGE_NEW:
                                if event.obj.peer_id == q:
                                    if event.obj.text == ans[i]:
                                        vk.messages.send(
                                                random_id=get_random_id(),
                                                peer_id = q,
                                                message= ('Yes'),
                                                )
                                    else:
                                        vk.messages.send(
                                                random_id=get_random_id(),
                                                peer_id = q,
                                                message= ('No \n'+ ans[i]),
                                                )
                                    break
                elif event.obj.text == 'Кто вор?' or event.obj.text == 'кто вор?' or event.obj.text == 'кто вор':
                    q = []
                    e = vk.messages.getConversationMembers(
                      peer_id = event.obj.peer_id,
                      )
                    for i in range(e['count']-1):
                        if ((e['profiles'])[i])['id']>0:
                            q.append((((e['profiles'])[i])['first_name']+ ' '+ ((e['profiles'])[i])['last_name']+" вор"))
                    vk.messages.send(
                          random_id=get_random_id(),
                          peer_id = event.obj.peer_id,
                          message=q[random.randint(0,len(q)-1)],
                          )

                elif event.obj.text == 'Погода' or event.obj.text == 'погода':
                    f1,f2,f3,f4,f5,f6 = weather()
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=f1,
                        keyboard = keyboard
                        )
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=f2
                        )
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=f3,
                        )
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=f4,
                        )
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=f5,
                        )
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        message=f6,
                        )
                    vk.messages.send(
                        random_id=get_random_id(),
                        peer_id = event.obj.peer_id,
                        sticker_id = 2049
                        )
                elif (event.obj.text) == 'Вики' or (event.obj.text) == 'вики':
                    vk.messages.send(
                                random_id=get_random_id(),
                                peer_id = event.obj.peer_id,
                                message='Введите запрос:'
                                )
                    q = event.obj.peer_id
                    for event in longpoll.listen():
                        if event.type == VkBotEventType.MESSAGE_NEW:
                            if event.obj.peer_id == q:
                                vk.messages.send(
                                        random_id=get_random_id(),
                                        peer_id = q,
                                        message= wiki(event.obj.text)
                                        )
                                vk.messages.send(
                                    random_id=get_random_id(),
                                    peer_id = event.obj.peer_id,
                                    sticker_id = 12948
                                    )
                                break
                elif event.obj.text == 'музыка для учебы' or event.obj.text == 'Музыка для учебы' or event.obj.text == 'музыка для учёбы' or event.obj.text == 'Музыка для учёбы' or event.obj.text == 'Муз' or event.obj.text == 'муз':
                    vk.messages.send(
                                random_id=get_random_id(),
                                peer_id = event.obj.peer_id,
                                message = 'Сколько песен? (не больше 10-ти)'
                                )
                    q = event.obj.peer_id
                    for event in longpoll.listen():
                        if event.type == VkBotEventType.MESSAGE_NEW:
                            if event.obj.peer_id == q:
                                for i in range(int(event.obj.text)):
                                    vk.messages.send(
                                        random_id=get_random_id(),
                                        peer_id = q,
                                        attachment='audio-71095783_' + rand_music()
                                        )
                                break
                elif event.obj.text == 'Привет' or event.obj.text == 'привет':
                    vk.messages.send(
                                random_id=get_random_id(),
                                peer_id = event.obj.peer_id,
                                message = 'Привет🙃 Я бот Heuchler и вот мои команды:\n1.Звонок - время до ближайшего звонка (в минутах)🔔\n2.Погода  - погода в городе Октябрьский🌥\n3.Вики - введите любой запрос и бот отправит вам статью из Википедии👔\n4.Музыка для учебы(Краткая команда - муз)🎶 '
                                )
                elif event.obj.text == 'расписание' or event.obj.text == 'Расписание':
                    vk.messages.send(
                                random_id=get_random_id(),
                                peer_id = event.obj.peer_id,
                                message = 'Введите ФИ',
                                keyboard = keyboard
                                )
                    q = event.obj.peer_id
                    for event in longpoll.listen():
                        if event.type == VkBotEventType.MESSAGE_NEW:
                            if event.obj.peer_id == q:
                                vk.messages.send(
                                        random_id=get_random_id(),
                                        peer_id = q,
                                        message= excel(event.obj.text)
                                        )
                                break
                elif event.obj.text == 'del':
                    vk.messages.send(
                                random_id=get_random_id(),
                                peer_id = event.obj.peer_id,
                                message = 'LoremIpsum',
                                keyboard = keyboard2
                                )
                elif event.obj.text in sub:
                    print(event.obj.text)
                    vk.messages.send(
                                random_id=get_random_id(),
                                peer_id = event.obj.peer_id,
                                message = ex(event.obj.text),
                                )
                elif event.obj.text == 'добро' or event.obj.text == 'Добро':
                    vk.messages.send(
                                        random_id=get_random_id(),
                                        peer_id = event.obj.peer_id,
                                        attachment='video-171434587_' + rand_video()
                                        )
                elif event.obj.text == 'ккк':
                    q = event.obj.peer_id
                    for event in longpoll.listen():
                        if event.type == VkBotEventType.MESSAGE_NEW:
                            if event.obj.peer_id == q:
                                vk.messages.send(
                                        random_id=get_random_id(),
                                        peer_id = q,
                                        message= excel(event.obj.text)
                                        )
                                break

if __name__ == '__main__':
    main()
